#!/usr/bin/python

import bittrex
from operator import itemgetter, attrgetter

# -- The flipper bot.
# 1. detect gaps in order book.


maxvolume = 40 #needs at least x btc / 24h

b = bittrex.bittrex("")

markets = b.get_market_summaries()

def int_getKey(item):
    return item[1]

interesting = []
print "Market,Ratio"
for market in  markets["result"]:
    if (market["BaseVolume"]) > maxvolume and "BTC" in market["MarketName"]:
        #print  market["MarketName"]+","+str(((market["Ask"] /  market["Bid"])-1)*100)
        interesting.append( [market["MarketName"], ((market["Ask"] /  market["Bid"])-1)*100, market["Ask"], market["Bid"], market["BaseVolume"] ] )

interesting = sorted(interesting, key=itemgetter(1))

for i in interesting:
    #print str(i[1])
    print i[0]+","+str(i[1])+","+str(i[2])+","+str(i[3])+","+str(i[4])
